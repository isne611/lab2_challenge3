#include <iostream>
#include <cstdlib>
#include <vector>
#include <ctime>
#include <algorithm>
#include "customer.cpp"

using namespace std;

//bubble sort to sort arrived time
void BubbleSort(vector<customer> &num)
{
	int i, j, flag = 1;
	customer temp;
	int numLength = num.size();
	for (i = 1; (i <= numLength) && flag; i++)
	{
		flag = 0;
		for (j = 0; j < (numLength - 1); j++)
		{
			if (num[j] > num[j + 1])
			{
				temp = num[j];
				num[j] = num[j + 1];
				num[j + 1] = temp;
				flag = 1;
			}
		}
	}
}


int main()
{
	srand(time(NULL));
	vector<customer> myqueue;
	customer temp;
	vector<int> wait, left, casheirqueue;
	int max_custom, min_custom, max_service, min_service, num_cash;
	float avg_wait;

	cout << "Enter MaxCustomers: ";
	cin >> max_custom;
	cout << "Enter MinCustomers: ";
	cin >> min_custom;
	cout << "Enter MaxServiceTime: ";
	cin >> max_service;
	cout << "Enter MinServiceTime: ";
	cin >> min_service;
	cout << "Enter Number of Casheir: ";
	cin >> num_cash;

	if (max_custom - min_custom == 0) //if max-min equal to zero function can't find num of customer
	{
		max_custom = max_custom + 1;
	}

	int numCustom = rand() % (max_custom - min_custom) + min_custom;

	int i;
	for (i = 0; i < numCustom; i++)
	{
		temp.set_both(min_service, max_service); //input value to customer class	
		myqueue.push_back(temp); //input value that set of temp to myqueue 
		//cout << myqueue.back().get_arr() << " " << myqueue.back().get_ser() << endl;
	}

	BubbleSort(myqueue); //sort who is arrived first 

	//cout << "----------------" << endl;

	/*for (i = 0; i < numCustom; i++)
	{
		cout << myqueue[i].get_arr() << " " << myqueue[i].get_ser() << endl;
	}*/

	i = 0;
	avg_wait = 0;
	while (i < numCustom) //do until equal customer amount
	{
		while (i < num_cash) //casheir available equal to number of customer 
		{
			wait.push_back(0); //no waiting time
			avg_wait = avg_wait + wait.at(i);
			left.push_back(myqueue[i].get_arr() + myqueue[i].get_ser());	//left time is arrived time + service time 
			casheirqueue.push_back(left.at(i));	//total time of casheir per 1 customer 
			sort(casheirqueue.begin(), casheirqueue.end()); //sort for find next fastest available casheir
			i++;
		}

		//cout << casheirqueue.front()<<"<-front "<< myqueue[i].get_arr(); cout << endl;
		if (casheirqueue.front() - myqueue[i].get_arr() < 0) //if casheir time minus customer time arrived less than 0 so customer dont need to wait
		{
			wait.push_back(0);//no waiting time
			avg_wait = avg_wait + wait.at(i);
			left.push_back(myqueue[i].get_arr() + myqueue[i].get_ser()); //it same from casheir no queue to do
		}
		else if (casheirqueue.front() - myqueue[i].get_arr() > 0) //casheir not available, customer need to wait
		{
			wait.push_back(casheirqueue.front() - myqueue[i].get_arr()); //wait time is equal to casheir time service before current customer minus current customer time arrived
			avg_wait = avg_wait + wait.at(i);
			left.push_back(myqueue[i].get_arr() + myqueue[i].get_ser() + wait.at(i)); //it same from customer dont need to wait but plus waiting time 
		}

		casheirqueue.push_back(left.at(i));	//total time per 1 customer for customer in if/else condition 
		casheirqueue.erase(casheirqueue.begin());	//clear casheir queue after service finished 
		sort(casheirqueue.begin(), casheirqueue.end()); //sort for find next fastest available casheir
		i++;

	}
	for (i = 0; i < numCustom; i++)
	{
		cout << "Customer " << i + 1 << " - arrived: " << myqueue[i].get_arr() << " - wait time " << wait.at(i) << " - left: " << left.at(i) << "." << endl;
	}
	cout << "Average wait time: " << (avg_wait / numCustom) << " minutes.";


	system("PAUSE");
}